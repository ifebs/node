# Check resolv.conf
RESOLV=$(cat /etc/resolv.conf | grep -oP '^search \K.*')
if [ "$RESOLV" != "" ] && [ "$RESOLV" != "." ]; then
 echo "Error in /etc/resolv.conf. Please change search to ."
 exit 0
fi

# Install docker
apt update
apt -y install apt-transport-https ca-certificates curl gnupg2 software-properties-common
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
echo "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list
apt update
apt install -y docker-ce
service docker start

# Install docker-compose 
curl -L "https://github.com/docker/compose/releases/download/1.9.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

# Giving non-root access (optional)
groupadd docker
gpasswd -a gerd docker
service docker restart
